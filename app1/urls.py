from django.contrib import admin
from django.urls import path,include
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('',views.loginf,name='loginf'),
    path('adminlogin',views.adminlogin,name='adminlogin'),
    path('logina',views.logina,name='logina'),
    path('dashboard',views.dashboard,name='dashboard'),
    ## user ##
    path('add',views.addUser,name='adduser'),
    path('listuser',views.listUser,name='listUser'),
    path('deleteUser<int:userid>',views.deleteUser,name='deleteUser'),
    # path('search',views.searchUser,name='searchUser'),
    # path('edit<int:userid>',views.editUser,name='editUser'),
    path('edit',views.editUser,name='editUser'),
    #path('savechanges',views.savechanges,name='savechanges'),
    ## estimate ##
    path('estimate',views.estimate,name='estimate'),
    path('listproject',views.listProject,name='listProject'),
    path('deleteProject<int:projectid>',views.deleteProject,name='deleteProject'),
    # path('searchProject',views.searchProject,name='searchProject'),
    path('editestimate',views.editestimate,name='editestimate'),
    
    #### client #######
    path('create_client', views.create_client, name='create_client'),
    path('listclient', views.listclient, name='listclient'),
    path('searchclient', views.searchclient, name='searchclient'),
    path('editclient',views.editClient,name='editClient'),
    path('deleteclient/<int:id>', views.deleteclient, name='deleteclient'),

    ### Total billing ###
    path('Totalbilling',views.Totalbilling,name='Totalbilling'),
    path('approve_all',views.approve_all,name="approve_all"),
    path('pending',views.pending,name="pending"),
    # path('generate',views.generate,name='generate'),
    path('accept/<int:task_id>',views.accept,name='accept'),
    ### payment ##
    path('payment<int:clientid>',views.payment,name='payment'),
    path('addPayment',views.addPayment,name='addPayment'),
    path('deletePayment<int:paymentid>',views.deletePayment,name='deletePayment'),
    path('editPayment',views.editPayment,name='editPayment'),
    path('BillingPerfom<int:userid>',views.BillingPerfom,name="BillingPerfom"),
    path('Incentive',views.Incentive,name='Incentive'),
    path('monthly',views.monthly,name="monthly"),
    path('expenses',views.expenses,name="expenses"),
    path('delete_expense<int:expenseid>',views.delete_expense,name="delete_expense"),

    ### engagement ##
    path('listEngagements',views.listEngagements,name='listEngagements'),
    path('create_engagement',views.create_engagement,name='create_engagement'),
    path('editengagement',views.editengagement,name='editengagement'),
    ### consultant ###
    path('listConsultant',views.listconsultant,name='listconsultant'),
    path('addConsultant',views.addConsultant,name='addConsultant'),
    path('editConsultant',views.editConsultant,name='editConsultant'),
    path('consultant_billing/<int:userid>',views.consultatntbilling,name='consultatntbilling'),
    path('approve_allConsultant/<int:userid>',views.approve_allConsultatnt,name='approve_allConsultatnt'),
    path('pendingc/<int:userid>',views.pendingc,name='pendingc'),
    path('estimateconsultant/<int:userid>',views.estimateconsultant,name='estimateconsultant'),
    path('generateEstimate/<int:userid>',views.generateEstimate,name='generateEstimate'),
    path('estimation/<int:userid>',views.estimation,name='estimation'),
    path('invoice/<int:id>',views.invoice,name='invoice'),
    path('GeneratePDF/<int:id>',views.GeneratePDF,name='GeneratePDF'),
    path('estimation_admin',views.estimation_admin,name='estimation_admin'),
    

]+ static(settings.STATIC_URL,document_root=settings.STATIC_ROOT)
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)