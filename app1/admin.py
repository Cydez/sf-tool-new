from django.contrib import admin
from .models import User,Estimate,Client,Totalbilling,Payment,Incentives,Expenditure,Engagement,Estimation,Productdetails,Certificate
# Register your models here.
admin.site.register(User)
admin.site.register(Estimate)
admin.site.register(Client)
admin.site.register(Totalbilling)
admin.site.register(Payment)
admin.site.register(Incentives)
admin.site.register(Expenditure)
admin.site.register(Engagement)
admin.site.register(Estimation)
admin.site.register(Productdetails)
admin.site.register(Certificate)