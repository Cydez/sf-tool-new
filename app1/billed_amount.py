from developers.models import Task

def billing():
    for i in Task.objects.all():
        if i.status == True:
            if i.amount:
                project = i.project
                if hasattr(project,'estimate'):
                    estimate = i.project.estimate
                    estimate.billed_amount += i.amount
                    estimate.save()
                print(project)

billing()

