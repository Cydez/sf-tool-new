from io import BytesIO
from django.http import HttpResponse
from django.template.loader import get_template
from xhtml2pdf import pisa
import mimetypes,datetime
import img2pdf
from io import BytesIO
from .models import Estimation,Certificate
from PIL import Image,ImageDraw,ImageFont
from django.core.files.base import ContentFile
from django.core.mail import EmailMultiAlternatives
from django.conf import settings
def render_to_pdf(template_src, context_dict={}):
    template = get_template(template_src)
    html = template.render(context_dict)
    result= BytesIO()
    pdf = pisa.pisaDocument(BytesIO(html.encode("ISO-8859-1")),result)
    if not pdf.err:
        return HttpResponse(result.getvalue(),content_type='application/pdf')
    return None
def date_convert(date):
    date_f=date.split('-')
    for i in range(len(date_f)):
        date_f[i]=int(date_f[i])
    date_f=datetime.datetime(*date_f)
    date_f=date_f.strftime("%b %e, %Y")

    return date_f
def topdf(sid):
    s=Estimation.objects.get(id=sid)
    path=s.image.path

    pdf_bytes = img2pdf.convert(path)

    filename=s.customerName+'.pdf'
    s.pdf.save(filename,ContentFile(pdf_bytes),save=False)
    s.save()

    return None
def printoncertificate(sid,k,a_dict):
    #Get student from db
    s=Estimation.objects.get(id=sid)
    pname=s.customerName.upper()

    #Fetch certificate from db
    c=Certificate.objects.get(id=1)
    (x, y) = (c.xvalue, c.yvalue)
    #Pillow
    

    certificate_image = Image.open(c.image)
    
    draw = ImageDraw.Draw(certificate_image)

    #Hardcoded values starts
    font=ImageFont.truetype('static/fonts/Lato/Lato-Regular.ttf',15)   
    font2=ImageFont.truetype('static/fonts/Lato/Lato-Regular.ttf', 14)
    font3=ImageFont.truetype('static/fonts/Lato/Lato-Regular.ttf', 14)
    font4=ImageFont.truetype('static/fonts/Lato/Lato-Regular.ttf', 14)
    font5=ImageFont.truetype('static/fonts/Lato/Lato-Regular.ttf', 15)   
    
    if font.getsize(pname)[0]<1710:
        w=font.getsize(pname)[0]
        lindent=(1570-w)/2
        x+=lindent
    if font2.getsize(k['reportNumber'])[0]<1120:
        w=font2.getsize(k['reportNumber'])[0]
        indent=(1120-w)/2
        course_xvalue=1850+indent
    
    fillcolor = "black"
    fillcolor1 ="red"
    # draw.text((15,330), "oct5,2021", font=font,fill=fillcolor1)
    

    draw.text((25,210),pname,font=font,fill=fillcolor)
    draw.text((500,192),k['reportNumber'],font=font2,fill=fillcolor)
    draw.text((499,212),k['date'],font=font2,fill=fillcolor)
    draw.text((497,232),k['expireyDate'],font=font2,fill=fillcolor)
    draw.text((500,252),k['amounts'],font=font,fill=fillcolor)
    draw.text((500,532),k['amounts'],font=font5,fill=fillcolor)
    draw.text((500,567),k['amounts'],font=font,fill=fillcolor)
    dict2 = {}
    for k in a_dict:
        dict2[k] = len(a_dict[k])
    first_pair= dict2['product']
    i=0
    y=330
    while i< first_pair:
        draw.text((15,y),a_dict['product'][i],font=font3,fill=fillcolor)
        draw.text((330,y),a_dict['quantity'][i],font=font3,fill=fillcolor)
        draw.text((450,y),a_dict['price'][i],font=font4,fill=fillcolor)
        draw.text((550,y),a_dict['amountss'][i],font=font4,fill=fillcolor)
        y += 20
        i += 1
    #Hardcoding ends
    
    temp_buffer = BytesIO()
    certificate_image.save(temp_buffer, certificate_image.format)
    
    certificate_name=s.customerName
    certificate_name+='.'+certificate_image.format

    s.image.save(certificate_name,ContentFile(temp_buffer.getvalue()),save=False)
    s.save()

    return s

    