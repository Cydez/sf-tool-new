from django.apps import AppConfig


class DevelopersConfig(AppConfig):
	name = 'developers'

	def ready(self):
		from .updater import start
		start()